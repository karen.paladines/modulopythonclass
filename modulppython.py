                       # Autora: "Karen Paladines Pereira"
                       # Email: "karen.paladines@unl.edu.ec"

# CREE  UN MÓDULO PYTHON  LLAMADO "geometria"

import math

class Punto:

    def _init_(self, x = 0, y = 0):
        self.x = x
        self.y = y

    def _str_(self):
        return "({}, {})".format(self.x, self.y)

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print("{} pertenece al cuadrante uno".format(self))

        elif self.x < 0 and self.y > 0:
            print("{} pertenece al cuadrante dos".format(self))

        elif self.x < 0 and self.y < 0:
            print("{} pertenece al cuadrante tres".format(self))

        elif self.x > 0 and self.y < 0:
            print("{} pertenece al cuadrante cuatro".format(self))

        elif self.x != 0 and self.y == 0:
            print("{} sitúado sobre el eje X".format(self))

        elif self.x == 0 and self.y != 0:
            print("{} sitúado sobre el eje Y".format(self))

        else:
            print("{} se encuentra sobre el origen".format(self))

    def vector(self, p):
        print("El vector entre {} y {} es ({}, {})".format(
            self, p, p.x - self.x, p.y - self.y) )

    def distancia(self, p):
        d = math.sqrt((p.x - self.x)*2 + (p.y - self.y)* 2)
        print("La distancia entre los puntos {} y {} es {}".format(self, p, d))


class Rectangulo:

    def _init_(self, PunInicial=Punto(), PunFinal=Punto()):
        self.Inicial = PunInicial
        self.Final = PunFinal

        # Hago los cálculos, pero no llamo los atributos igual
        # que los métodos porque sino podríamos sobreescribirlos
        self.Base = abs(self.PunFinal.x - self.PunInicial.x)
        self.Altura = abs(self.PunFinal.y - self.PunInicial.y)
        self.Area = self.Base * self.Altura

    def base(self):
        print("La base del rectángulo es {}".format( self.vBase ) )

    def altura(self):
        print("La altura del rectángulo es {}".format( self.vAltura ) )

    def area(self):
        print("El área del rectángulo es {}".format( self.vArea ) )


A = Punto(2, 3)
B = Punto(5, 5)
C = Punto(-3, -1)
D = Punto(0, 0)

A.cuadrante()
C.cuadrante()
D.cuadrante()

A.vector(B)
B.vector(A)

A.distancia(B)
B.distancia(A)

A.distancia(D)
B.distancia(D)
C.distancia(D)

R = Rectangulo(A, B)
R.base()
R.altura()
R.area()